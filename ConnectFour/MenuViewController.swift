//
//  MenuViewController.swift
//  ConnectFour
//
//  Created by Thomas Donohue on 4/4/16.
//  Copyright © 2016 rsangar1@binghamton.edu. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

     @IBAction func startGameSegue(sender: AnyObject) {
        performSegueWithIdentifier("startGame", sender: sender)
     }
}

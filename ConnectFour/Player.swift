//
//  Player.swift
//  ConnectFour
//
//  Created by Thomas Donohue on 4/2/16.
//  Copyright © 2016 rsangar1@binghamton.edu. All rights reserved.
//

import UIKit
import GameplayKit

class Player: NSObject, GKGameModelPlayer {
    
    let pieceColor: PieceColor
    let playerColor: UIColor
    let playerName: String
    let playerId: Int
    
    init(pieceC: PieceColor) {
        self.pieceColor = pieceC
        // conform to GKGameModelPlayer
        self.playerId = pieceC.rawValue
        
        if pieceC == .Color1 {
            playerColor = .redColor()
            playerName = "Red"
        } else {
            playerColor = .yellowColor()
            playerName = "Yellow"
        }
        
        super.init()
    }
    
    static var allPlayers = [Player(pieceC: .Color1), Player(pieceC: .Color2)]
    
    var playerOpp: Player {
        if pieceColor == .Color1 {
            return Player.allPlayers[1]
        } else {
            return Player.allPlayers[0]
        }
    }


}

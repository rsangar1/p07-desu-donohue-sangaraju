//
//  ViewController.swift
//  ConnectFour
//
//  Created by Thomas Donohue on 4/1/16.
//  Copyright © 2016 CS441. All rights reserved.
//

import UIKit
import GameplayKit
import AVFoundation

enum PieceColor: Int {
    case Empty = 0
    case Color1
    case Color2
}

class GameViewController: UIViewController {
    
    @IBOutlet var columnButtons: [UIButton]!
    
    var placedPieces = [[UIView]]()
    var gameBoard: Board!
    var maxLookAheadDepth = 7
    
    //for various sounds
    var player1Beep : AVAudioPlayer?
    var player2Beep : AVAudioPlayer?
    var backgroundMusic : AVAudioPlayer?
    
    
    // https://developer.apple.com/library/ios/documentation/GameplayKit/Reference/GKMinmaxStrategist_Class/
    var moveStrategistAI: GKMinmaxStrategist!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        for _ in 0 ..< Board.boardWidth {
            placedPieces.append([UIView]())
        }
        
        
        moveStrategistAI = GKMinmaxStrategist()
        moveStrategistAI.maxLookAheadDepth = maxLookAheadDepth
        moveStrategistAI.randomSource = nil
        // ios9 - select random best move
        // moveStrategistAI.randomSource = GKARC4RandomSource()
        
        resetGameBoard()
        
        
        //different sounds that we use in this game
        if let player1Beep = self.setupAudioPlayerWithFile("player1", type:"mp3") {
            self.player1Beep = player1Beep
        }
        if let player2Beep = self.setupAudioPlayerWithFile("player2", type:"mp3") {
            self.player2Beep = player2Beep
        }
        /*if let backgroundMusic = self.setupAudioPlayerWithFile("HallOfTheMountainKing", type:"mp3") {
            self.backgroundMusic = backgroundMusic
        }*/

        
    }
    
    
    
    //this function will setup Audio Player and return it
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkMove(col : Int, row: Int, color:UIColor) {
        var gameOverTitle: String? = nil
        
        //if checkWinner(col,row: row, color:color) {
        // print("checkMove")
            
        if(gameBoard.playerWins(gameBoard.currPlayer)) {
            gameOverTitle = "\(gameBoard.currPlayer.playerName) Wins"
        } else if gameBoard.boardFull() {
            gameOverTitle = "Tie game"
        }
        
        if gameOverTitle != nil {
            /**
             let alertController = UIAlertController(title: "iOScreator", message:
             "Hello, world!", preferredStyle: UIAlertControllerStyle.Alert)
             alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
             self.presentViewController(alertController, animated: true, completion: nil)**/
            let alert = UIAlertController(title: gameOverTitle, message: nil, preferredStyle: .Alert)
            let alertAction = UIAlertAction(title: "Play Again", style: .Default) { [unowned self] (action) in
                self.resetGameBoard()
            }
            alert.addAction(alertAction)
            presentViewController(alert, animated: true, completion: nil)
            
            return
        }
        
        gameBoard.currPlayer = gameBoard.currPlayer.playerOpp
        updateTurnMessage()
    }
    
    func updateTurnMessage() {
        // shows at top of screen
        title = "\(gameBoard.currPlayer.playerName) Turn"
        
        if gameBoard.currPlayer.pieceColor == .Color2 {
            runAI()
        }
    }
    
    
    func resetGameBoard() {
        gameBoard = Board()
        moveStrategistAI.gameModel = gameBoard
        
        updateTurnMessage()
        
        for i in 0 ..< placedPieces.count {
            for piece in placedPieces[i] {
                piece.removeFromSuperview()
            }
            
            placedPieces[i].removeAll(keepCapacity: true)
        }
    }
    
    @IBAction func makeMove(sender: UIButton) {
        let col = sender.tag
        
        if let row = gameBoard.nextEmptySlotInCol(col) {
            gameBoard.addPiece(gameBoard.currPlayer.pieceColor, column: col)
            gameBoard.currentCol = col
            gameBoard.currentRow = row
            addPieceAtCol(col, row: row, color: gameBoard.currPlayer.playerColor)
            player1Beep?.play()
            checkMove(col, row: row, color: gameBoard.currPlayer.playerColor)
        }
    }
    
    func makeAIMoveInCol(col: Int) {
        if let row = gameBoard.nextEmptySlotInCol(col) {
            gameBoard.addPiece(gameBoard.currPlayer.pieceColor, column: col)
            addPieceAtCol(col, row: row, color: gameBoard.currPlayer.playerColor)
            gameBoard.currentCol = col
            gameBoard.currentRow = row
            player2Beep?.play()
            checkMove(col, row: row, color:gameBoard.currPlayer.playerColor)
        }
        columnButtons.forEach {
            $0.enabled = true
        }
        navigationItem.leftBarButtonItem = nil
    }
    
    func runAI() {
        columnButtons.forEach {
            $0.enabled = false
        }
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        spinner.startAnimating()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: spinner)
        
        //  https://thatthinginswift.com/background-threads/
        // run in background thread
        dispatch_async(dispatch_get_main_queue(), {
            if let aiMove = self.moveStrategistAI.bestMoveForPlayer(self.gameBoard.currPlayer) as? Move {
                self.makeAIMoveInCol(aiMove.column)
            }
        })
        
    }
    
    func positionForPieceAtCol(column: Int, row: Int) -> CGPoint {
        let button = columnButtons[column]
        let size = min(button.frame.size.width, button.frame.size.height / 6)
        
        let xOffset = CGRectGetMidX(button.frame)
        var yOffset = CGRectGetMaxY(button.frame) - size / 2
        yOffset -= size * CGFloat(row)
        return CGPointMake(xOffset, yOffset)
    }
    
    func addPieceAtCol(column: Int, row: Int, color: UIColor) {
        let button = columnButtons[column]
        let pieceSize = min(button.frame.size.width, button.frame.size.height / 6)
        
        if (placedPieces[column].count < row + 1) {
            let newPiece = UIView()
            //   newPiece.frame = CGRect(x: 0, y: 0, width: pieceSize, height: pieceSize)
            newPiece.frame = CGRect(x: 0, y: 0, width: pieceSize, height: pieceSize)
            newPiece.userInteractionEnabled = false
            newPiece.backgroundColor = color
            newPiece.layer.cornerRadius = pieceSize / 2
            newPiece.center = positionForPieceAtCol(column, row: row)
            // newPiece.transform = CGAffineTransformMakeTranslation(0, -400)
            newPiece.transform = CGAffineTransformMakeTranslation(0, -800)
            view.addSubview(newPiece)
            
            UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseIn, animations: { () -> Void in
                newPiece.transform = CGAffineTransformIdentity
                }, completion: nil)
            
            placedPieces[column].append(newPiece)
        }
    }
    //to check for winner
    func checkWinner(col:Int, row:Int, color:UIColor) -> Bool{
        //NSLog("inside check winner")
        return verticalCheck(col, row: row, color:color) || horizontalCheck(col, row: row, color:color) || leftDiagonalCheck(col, row:row, color:color) || rightDiagonalCheck(col, row: row, color:color)
    }
    //vertical checking
    func verticalCheck(col: Int, row: Int, color:UIColor) -> Bool {
        //var color = UIColor()
        var counter = 1
        //color = placedPieces[col][row].backgroundColor!
        NSLog("ROW VALUES:%d", row)
        if( row < 3 ){
            return false;
        }
        
        var temp = row
        for var k=1; k<4;++k{
            temp = row-k
            if(temp < 0) {
                break
            }
            else if(color != placedPieces[col][temp].backgroundColor){
                break
            }
            counter = counter + 1
        }
        NSLog("COUTNER:%d", counter)
        if(counter >= 4) {
            NSLog("vertical check");
            return true;
        }
        return false;
    }
    //horizontal checking
    func horizontalCheck(col: Int, row:Int, color:UIColor) -> Bool{
        var counter = 1;
        //var color = UIColor()
        var temp = col
        //color = placedPieces[col][row].backgroundColor!
        
        for var k=1; k<4; ++k {
            temp = col-k
            if(temp < 0) {
                break
            }
            else if(placedPieces[temp].count-1 < row) {
                break
            }
            else if(color != placedPieces[temp][row].backgroundColor) {
                break
            }
            else {
                counter = counter + 1
            }
        }
        
        temp = col
        
        for var k=1; k<4; ++k {
            temp = col + k
            
            if(temp > 6 ){
                break
            }
            else if (placedPieces[temp].count-1 < row) {
                break
            }
            else if (color != placedPieces[temp][row].backgroundColor) {
                break;
            }
            else {
                counter = counter + 1
            }
        }
        if(counter >= 4){
            NSLog("Horizontal check")
            return true;
        }
        return false;
    }
    func leftDiagonalCheck(col: Int, row:Int, color:UIColor)->Bool {
        var counter = 1;
        //var color = UIColor()
        var tempC = col
        var tempR = row
        //color = placedPieces[col][row].backgroundColor!
        
        for var k=1; k<4; ++k {
            tempC = col-k
            tempR = row+k
            if(tempC < 0 || row > 5) {
                break
            }
            else if(placedPieces[tempC].count-1 <= row+k) {
                break
            }
            else if(color != placedPieces[tempC][tempR].backgroundColor) {
                break
            }
            else {
                counter = counter + 1
            }
        }
        
        tempC = col
        tempR = row
        for var k=1; k<4; ++k {
            tempC = col + k
            tempR = row - k
            if(tempC > 6  || tempR < 0){
                break
            }
            else if (placedPieces[tempC].count-1 < row-k) {
                break
            }
            else if (color != placedPieces[tempC][tempR].backgroundColor) {
                break;
            }
            else {
                counter = counter + 1
            }
        }
        if(counter >= 4){
            return true;
        }
        return false;
        
    }
    
    func rightDiagonalCheck(col: Int, row:Int, color:UIColor) -> Bool {
        var counter = 1;
        //var color = UIColor()
        var tempC = col
        var tempR = row
        //color = placedPieces[col][row].backgroundColor!
        
        for var k=1; k<4; ++k {
            tempC = col+k
            tempR = row+k
            if(tempC > 6 || row > 5) {
                break
            }
            else if(placedPieces[tempC].count <= row+k) {
                break
            }
            else if(color != placedPieces[tempC][tempR].backgroundColor) {
                break
            }
            else {
                counter = counter + 1
            }
        }
        
        tempC = col
        tempR = row
        NSLog("COMING TO SECOND FOR")
        for var k=1; k<4; ++k {
            tempC = col - k
            tempR = row - k
            if(tempC < 0   || tempR < 0){
                break
            }
            else if (placedPieces[tempC].count-1 < row-k) {
                break
            }
            else if (color != placedPieces[tempC][tempR].backgroundColor) {
                break;
            }
            else {
                counter = counter + 1
            }
        }
        //NSLog("COUTNER:%d", counter)
        if(counter >= 4){
            //NSLog("Diagonal check")
            return true;
        }
        return false
    }
    
}


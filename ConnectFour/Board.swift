//
//  Board.swift
//  ConnectFour
//
//  Created by Thomas Donohue on 4/2/16.
//  Copyright © 2016 CS441. All rights reserved.
//

import UIKit
import GameplayKit

class Board: NSObject, GKGameModel {
    static let boardWidth = 7
    static let boardHeight = 6
    
    var colSlots = [PieceColor]()
    
    // mutable
    var currPlayer: Player
    
    var currentRow = 0;
    var currentCol = 0;
    
    //init Board
    override init() {
        currPlayer = Player.allPlayers[0]
        
        for _ in 0 ..< Board.boardWidth * Board.boardHeight {
            colSlots.append(.Empty)
        }
        
        super.init()
    }

    
    func pieceInCol(column: Int, row: Int) -> PieceColor {
        let index = row + column * Board.boardHeight
        return colSlots[index]
    }
    
    func setPiece(piece: PieceColor, column col: NSInteger, row: NSInteger) {
        let index = row + col * Board.boardHeight
        colSlots[index] = piece;
    }
    
    // get the next available slot
    func nextEmptySlotInCol(column: Int) -> Int? {
        //for _ in 0 ..< Board.boardHeight {
        for row in 0 ..< Board.boardHeight {
            if pieceInCol(column, row: row) == .Empty {
                return row
            }
        }
        
        return nil;
    }
    
    // see if it's possible to place in column
    func canMoveInCol(column: Int) -> Bool {
        return nextEmptySlotInCol(column) != nil
    }
    
    // add actual piece
    func addPiece(piece: PieceColor, column col: Int) {
        //   setPiece(chip, column: column, row: row)
        if let row = nextEmptySlotInCol(col) {
            setPiece(piece, column: col, row: row)
        }
    }
    
    // checks for possible win given a piece on the board
    func checkFour(piece: PieceColor, row: Int, col: Int, moveX: Int, moveY: Int) -> Bool {
        // cannot be 4 in a row
        if row + (moveY * 3) < 0 {
            return false
        }
        if row + (moveY * 3) >= Board.boardHeight {
            return false
        }
        if col + (moveX * 3) < 0 {
            return false
        }
        if col + (moveX * 3) >= Board.boardWidth {
            return false
        }
        
        // check each spot
        if pieceInCol(col, row: row) != piece {
            return false
        }
        if pieceInCol(col + moveX, row: row + moveY) != piece {
            return false
        }
        if pieceInCol(col + (moveX * 2), row: row + (moveY * 2)) != piece {
            return false
        }
        if pieceInCol(col + (moveX * 3), row: row + (moveY * 3)) != piece {
            return false
        }
        
        return true
    }
    
    func checkThree(piece: PieceColor, row: Int, col: Int, moveX: Int, moveY: Int) -> Bool {
        if row + (moveY * 2) < 0 {
            return false
        }
        if row + (moveY * 2) >= Board.boardHeight {
            return false
        }
        if col + (moveX * 2) < 0 {
            return false
        }
        if col + (moveX * 2) >= Board.boardWidth {
            return false
        }
        
        // check each spot
        if pieceInCol(col, row: row) != piece {
            return false
        }
        if pieceInCol(col + moveX, row: row + moveY) != piece {
            return false
        }
        if pieceInCol(col + (moveX), row: row + (moveY)) != piece {
            return false
        }

        
        return true
    }
    
    func numberofThreeInARow(player: GKGameModelPlayer) -> Int {
        let piece = (player as! Player).pieceColor
        
        var num = 0;
        
        for row in 0 ..< Board.boardHeight {
            for col in 0 ..< Board.boardWidth {
                if checkThree(piece, row: row, col: col, moveX: 1, moveY: 0) {
                    num++
                } else if checkThree(piece, row: row, col: col, moveX: 0, moveY: 1) {
                    num++
                } else if checkThree(piece, row: row, col: col, moveX: 1, moveY: 1) {
                    num++
                } else if checkThree(piece, row: row, col: col, moveX: 1, moveY: -1) {
                    num++
                }
            }
        }
        return num
        
    }
    
    
    
    func boardFull() -> Bool {
        for col in 0 ..< Board.boardWidth {
            if canMoveInCol(col) {
                return false
            }
        }
        
        return true
    }
    
    func playerWins(player: GKGameModelPlayer) -> Bool {
       let piece = (player as! Player).pieceColor
        
       for row in 0 ..< Board.boardHeight {
            for col in 0 ..< Board.boardWidth {
                if checkFour(piece, row: row, col: col, moveX: 1, moveY: 0) {
                    //NSLog("a");
                    return true
                } else if checkFour(piece, row: row, col: col, moveX: 0, moveY: 1) {
                    //NSLog("b");
                    return true
                } else if checkFour(piece, row: row, col: col, moveX: 1, moveY: 1) {
                    //NSLog("c");
                    return true
                } else if checkFour(piece, row: row, col: col, moveX: 1, moveY: -1) {
                    //NSLog("d");
                    return true
                }
            }
        }
        return false

        //return GameViewController().checkWinner(currentCol, row: currentRow, color:currPlayer.playerColor);
    }
    
    
    // https://developer.apple.com/library/ios/documentation/Cocoa/Reference/Foundation/Protocols/NSCopying_Protocol/index.html#//apple_ref/occ/intf/NSCopying
    // NSCopying
    func copyWithZone(zone: NSZone) -> AnyObject {
        let copyB = Board()
        copyB.setGameModel(self)
        return copyB
    }
    
    // https://developer.apple.com/library/ios/documentation/GameplayKit/Reference/GKGameModel_Protocol/
    //---------------------------------GKGameModel------------------------------------
    
    var players: [GKGameModelPlayer]? {
        return Player.allPlayers
    }
    
    var activePlayer: GKGameModelPlayer? {
        return currPlayer
    }
    
    
    // Required
    func gameModelUpdatesForPlayer(player: GKGameModelPlayer) -> [GKGameModelUpdate]? {
        //print("gameModelUpdatesForPlayer")
        if let playerObj = player as? Player {
            if playerWins(playerObj) || playerWins(playerObj.playerOpp) {
                return nil
            }
            
            var moves = [Move]()
            
            for col in 0 ..< Board.boardWidth {
                if canMoveInCol(col) {
                    moves.append(Move(column: col))
                }
            }
            
            return moves;
        }
        
        return nil
    }
    
    // Needed for min max
    func scoreForPlayer(player: GKGameModelPlayer) -> Int {
        if let playerObj = player as? Player {
             if isWinForPlayer(playerObj) {
                // print("scoreForPlayer: 1000")
                return 1000
            } else if isWinForPlayer(playerObj.playerOpp) {
                // print("scoreForPlayer: -1000")
                return -1000
            }
        }
        // print("scoreForPlayer: 0")
        return 0
    }
    
    func isWinForPlayer(player: GKGameModelPlayer) -> Bool {
        return playerWins(player)
    }
    
    // Required
    func applyGameModelUpdate(gameModelUpdate: GKGameModelUpdate) {
        if let move = gameModelUpdate as? Move {
            addPiece(currPlayer.pieceColor, column: move.column)
            currPlayer = currPlayer.playerOpp
        }
    }
    
    // Required
    func setGameModel(gameModel: GKGameModel) {
        if let tBoard = gameModel as? Board {
            colSlots = tBoard.colSlots
            currPlayer = tBoard.currPlayer
        }
    }
    

    
    
    
}

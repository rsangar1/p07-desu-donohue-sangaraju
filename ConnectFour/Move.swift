//
//  Move.swift
//  ConnectFour
//
//  Created by Thomas Donohue on 4/5/16.
//  Copyright © 2016 rsangar1@binghamton.edu. All rights reserved.
//

import UIKit
import GameplayKit


class Move: NSObject, GKGameModelUpdate {
    var value: Int = 0
    var column: Int
    
    init(column: Int) {
        self.column = column
    }
}